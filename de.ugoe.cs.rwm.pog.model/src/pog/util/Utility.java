package pog.util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import pog.Graph;
import pog.PogPackage;

public class Utility {
	/**
	 * Loads POG model as List of EObjects.
	 *
	 * @param path
	 *            to the POG model
	 * @return POG model as List of EObjects
	 */
	public static EList<EObject> loadPOG(Path path) {
		PogPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("pog", new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		// URI fileURI = URI.createURI(path.toString());
		URI fileURI = URI.createFileURI(path.toString());
		Resource resource = resSet.getResource(fileURI, true);

		return resource.getContents();
	}

	/**
	 * Stores POG at the given Path path.
	 *
	 * @param path
	 *            Path in which the POG gets stored.
	 * @param graph
	 *            Toplevel element of the POG.
	 */
	public static void storePOG(Path path, Graph graph) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("pog", new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		URI fileURI = URI.createFileURI(path.toString());
		Resource resource = resSet.createResource(fileURI);
		resource.getContents().add(graph);

		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
